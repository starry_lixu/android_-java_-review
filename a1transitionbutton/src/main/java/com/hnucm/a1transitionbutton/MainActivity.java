package com.hnucm.a1transitionbutton;

import android.animation.Animator;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.hnucm.a1transitionbutton.button.MyButton;


public class MainActivity extends AppCompatActivity {
    private MyButton button;
    private RelativeLayout rlContent;
    private Handler handler;
    private Animator animator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button=findViewById(R.id.button_test);
        rlContent=findViewById(R.id.rl_content);

        rlContent.getBackground().setAlpha(0);
        handler=new Handler();

        button.setOnClickListener(v -> {
            button.startAnim();

            handler.postDelayed(() -> {
                //跳转
                final Intent intent=new Intent(this, Main2Activity.class);

                startActivity(intent);
//                gotoNew();
            },3000);

        });
    }



}