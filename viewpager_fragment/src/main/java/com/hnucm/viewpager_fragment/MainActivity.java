package com.hnucm.viewpager_fragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.Toast;

import com.hnucm.viewpager_fragment.adapter.MyFragmentVPAdapter;
import com.hnucm.viewpager_fragment.fragment.MyFragment;

import java.util.ArrayList;
import java.util.List;

/**ViewPager与Fragment结合实现多页面滑动
 * 主要使用到了FragmentPagerAdapter用于连接ViewPager与Fragment的桥梁
 * 复习ViewPager实现起始页
 * ---数据List+适配器Adapter+ViewPager
 * FragmentPagerAdapter的用法
 * 1.重写getItem()  获取fragment
 * 2.重写getCount()  返回fragment的实例个数
 *
 * 具体实现
 * 1.设置布局，并给viewPager控件设置id
 * 2.获取到viewPager实例
 * 3.创建fragment页面（初始化数据）
 * 4.创建一个自定义的适配器myFragmentVPAdapter
 * 5.初始化数据
 * 6.获取MyFragmentVPAdapter实例，需要传入两个参数
 * 7.将ViewPager和myFragmentVPAdapter适配器绑定
 * 8.实现事件监听，和上一个demo一样
 */

/**
 * ViewPager切换页面与底部导航按钮的联动
 */
public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private List<Fragment> fragmentList;
    private MyFragmentVPAdapter myFragmentVPAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        获取到viewPager实例
        viewPager=findViewById(R.id.viewpager);
//        初始化数据
        initData();
//        获取MyFragmentVPAdapter实例，需要传入两个参数
        myFragmentVPAdapter=new MyFragmentVPAdapter(getSupportFragmentManager(),fragmentList);
//        将ViewPager和myFragmentVPAdapter适配器绑定
        viewPager.setAdapter(myFragmentVPAdapter);
//        实现事件监听，和上一个demo一样
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Toast.makeText(MainActivity.this,"这是第"+(position+1)+"个fragment",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initData() {
        fragmentList=new ArrayList<>();
        MyFragment fragment1=MyFragment.newInstance("这是第1个fragment");
        MyFragment fragment2=MyFragment.newInstance("这是第2个fragment");
        MyFragment fragment3=MyFragment.newInstance("这是第3个fragment");
        MyFragment fragment4=MyFragment.newInstance("这是第4个fragment");
        MyFragment fragment5=MyFragment.newInstance("这是第5个fragment");
        fragmentList.add(fragment1);
        fragmentList.add(fragment2);
        fragmentList.add(fragment3);
        fragmentList.add(fragment4);
        fragmentList.add(fragment5);

    }
}