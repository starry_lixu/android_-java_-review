package com.hnucm.viewpager_fragment.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * 1.自定义一个适配器类MyFragmentVPAdapter继承自FragmentPagerAdapter
 * 2.重写getItem()和getCount()方法
 * 3.在构造方法中初始化数据
 * 4.getItem返回当前的fragment
 * 5.getCount返回fragment的数量
 */
public class MyFragmentVPAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragmentList;
    public MyFragmentVPAdapter(@NonNull FragmentManager fm,List<Fragment> fragmentList) {
        super(fm);
        this.fragmentList=fragmentList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragmentList==null?null:fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList==null?0:fragmentList.size();
    }
}
