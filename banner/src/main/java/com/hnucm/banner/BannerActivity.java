package com.hnucm.banner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.youth.banner.Banner;

import java.util.ArrayList;
import java.util.List;

public class BannerActivity extends AppCompatActivity {

    Banner banner;
    List<String> picList=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner);

        banner=findViewById(R.id.banner1);
        picList.add("https://pic1.zhimg.com/v2-d58ce10bf4e01f5086c604a9cfed29f3_r.jpg?source=1940ef5c");
        picList.add("https://www.keaidian.com/uploads/allimg/190424/24110307_19.jpg");
        picList.add("https://tse1-mm.cn.bing.net/th/id/OIP-C.nRlAFygdctTCHmIWN7GxRwHaEK?pid=ImgDet&rs=1");
    }
}