# Android_Java_Review

# 1.介绍
学习--代码即笔记

# 2.Android Studio

![image-20221111172253231](https://starry-lixu.oss-cn-hangzhou.aliyuncs.com/202211111722275.png)

![image-20221111172313684](https://starry-lixu.oss-cn-hangzhou.aliyuncs.com/202211111723721.png)

![image-20221111172333551](https://starry-lixu.oss-cn-hangzhou.aliyuncs.com/202211111723617.png)

# 3.Module介绍

## app：空项目

## tablayout：空项目

## viewpager

ViewPager实现引导动画的效果

## viewpager_fragment：ViewPager与Fragment结合实现多页面滑动

## viewpager_fragment_bottomnavigationview：

ViewPager+Fragment+BottomNavigationView实现底部导航以及Fragment+ViewPager+TabLayout实现主页的顶部页面导航

## banner简单实现轮播图：Banner控件的简单使用（暂时未完）

## materialdesign：

- NavigationView实现菜单选项
- drawerlayout侧边栏抽屉布局
- Toolbar自定义ActionBar

![Ad_003](https://starry-lixu.oss-cn-hangzhou.aliyuncs.com/202211142236578.gif)

## materialdesign2：

- CoordinatorLayout布局
- FloatingActionButton浮动按钮
- CoordinatorLayout+AppBarLayout+CollapsingToolbarLayout+Toolbar实现吸顶效果



## tablayout

tablayout实现登录注册（未完）

## a1transitionbutton

自定义Button实现动画效果（失败，背景都会变成黑色）

![D:\nodepicgo\ScreenToGifTemp\wrokspace](https://starry-lixu.oss-cn-hangzhou.aliyuncs.com/202211142233301.gif)

## a4textinputlayout
带动画的输入控件和布局
TextInputLayout