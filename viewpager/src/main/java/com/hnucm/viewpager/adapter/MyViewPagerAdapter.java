package com.hnucm.viewpager.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import java.util.List;
//adapter与viewpager的关系  adapter是将数据渲染到视图的桥梁

/**
 * 1.继承自PagerAdapter抽象类
 * 2.重写抽象类中的方法getCount()和isViewFromObject()
 * 3.ctrl+o重写  生成:instantiateItem()  销毁:destroyItem()
 * 4.用一个imageViewList数组来存储图片数据,并创建构造函数初始化
 * 5.getCount()方法返回图片的数量
 * 6.instantiateItem()将图片数据加载到viewpager页面,返回加载的这个图片
 * 7.destroyItem()将图片从viewpager中移除
 *
 * 注意：需要为viewpager设置适配器，
 * 适配器继承PagerAdapter，
 * 并重写getCount、isViewFromObject、instantiateItem和destroyItem四个方法。
 */
public class MyViewPagerAdapter extends PagerAdapter {

    private List<ImageView> imageViewList;

    public MyViewPagerAdapter(List<ImageView> imageViewList) {
        this.imageViewList = imageViewList;
    }

    @Override
    public int getCount() {
        //判空处理
        return imageViewList == null ? 0 : imageViewList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        //view和object是同一个页面实例
        return view==object;
    }

    //将图片数据加载到viewpager页面中，返回加载的这个图片
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView=imageViewList.get(position);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //将图片从viewpager中移除
        container.removeView((View) object);
    }
}
