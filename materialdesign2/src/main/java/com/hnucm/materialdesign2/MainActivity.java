package com.hnucm.materialdesign2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.snackbar_action).setOnClickListener(v -> {
            Intent intent=new Intent(MainActivity.this,SnackbarActivity.class);
            startActivity(intent);
        });

        findViewById(R.id.collapse_action).setOnClickListener(v -> {
            Intent intent=new Intent(MainActivity.this,CollapseActivity.class);
            startActivity(intent);
        });
    }
}