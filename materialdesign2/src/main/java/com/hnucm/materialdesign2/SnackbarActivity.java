package com.hnucm.materialdesign2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

public class SnackbarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snackbar);
        findViewById(R.id.fab).setOnClickListener(v -> {
            Snackbar.make(v,"Snackbar FAB Clicked",Snackbar.LENGTH_SHORT).
                    setAction("Confirm", v1 ->
                            Toast.makeText(SnackbarActivity.this,
                                    "Confirm Clicked",Toast.LENGTH_SHORT).show())
                    .show();
        });
    }
}