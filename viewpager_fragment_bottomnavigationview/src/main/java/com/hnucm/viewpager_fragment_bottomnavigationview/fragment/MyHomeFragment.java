package com.hnucm.viewpager_fragment_bottomnavigationview.fragment;

import android.icu.text.CaseMap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.hnucm.viewpager_fragment_bottomnavigationview.R;
import com.hnucm.viewpager_fragment_bottomnavigationview.adapter.MyHomeFragmentSVpTitleAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 其实主页就是一个Fragment里又嵌套了ViewPager，
 * 所以我们为了区别于其他三个模块，我不再使用MyFragment，
 * 而是新建一个MyHomeFragment，在其中我联动TabLyout再嵌套ViewPager
 * 其实与再MainActivity中使用ViewPager类似
 * 这里我们只是在Fragment里使用ViewPager.所以思路是一样的
 * 不同的地方可能就在于TabLayout和ViewPager的联动，但这个实现就一行代码
 * 1.准备工作：创建布局，设置好TabLayout和ViewPager
 * 2.在MyHomeFragment中初始化控件
 * 3.创建MyHomeFragmentSVpTitleAdapter，同样的需要适配器
 * 4.初始化数据，绑定myHomeFragmentSVpTitleAdapter与viewPager
 * 5.TabLayout和ViewPager的联动
 */
public class MyHomeFragment extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<Fragment> fragmentList;
    private List<String> titleList;
    private MyHomeFragmentSVpTitleAdapter myHomeFragmentSVpTitleAdapter;
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private String mParam1;

    public MyHomeFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MyHomeFragment newInstance(String param1) {
        MyHomeFragment fragment = new MyHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        获取控件
        viewPager=view.findViewById(R.id.viewpagerhome);
        tabLayout=view.findViewById(R.id.tablayout);
//        初始化数据
        initData();
//        获取适配器实例,注意区别这里传入的是getChildFragmentManager()
        myHomeFragmentSVpTitleAdapter=new MyHomeFragmentSVpTitleAdapter(getChildFragmentManager(),fragmentList,titleList);
//        绑定myHomeFragmentSVpTitleAdapter与viewPager
        viewPager.setAdapter(myHomeFragmentSVpTitleAdapter);
//        通过这个setupWithViewPager 方法设置一下，他们就可以实现TabLayout与ViewPager互相变换，
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initData() {
        fragmentList=new ArrayList<>();
        MyFragment fragment1=MyFragment.newInstance("推荐");
        MyFragment fragment2=MyFragment.newInstance("关注");
        MyFragment fragment3=MyFragment.newInstance("娱乐");
        MyFragment fragment4=MyFragment.newInstance("军事");
        MyFragment fragment5=MyFragment.newInstance("新闻");
        MyFragment fragment6=MyFragment.newInstance("文化");
        MyFragment fragment7=MyFragment.newInstance("科技");
        fragmentList.add(fragment1);
        fragmentList.add(fragment2);
        fragmentList.add(fragment3);
        fragmentList.add(fragment4);
        fragmentList.add(fragment5);
        fragmentList.add(fragment6);
        fragmentList.add(fragment7);

        titleList=new ArrayList<>();
        titleList.add("推荐");
        titleList.add("关注");
        titleList.add("娱乐");
        titleList.add("军事");
        titleList.add("新闻");
        titleList.add("文化");
        titleList.add("科技");
    }
}