package com.hnucm.viewpager_fragment_bottomnavigationview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.hnucm.viewpager_fragment_bottomnavigationview.adapter.MyFragmentStateVPAdapter;
import com.hnucm.viewpager_fragment_bottomnavigationview.fragment.MyFragment;
import com.hnucm.viewpager_fragment_bottomnavigationview.fragment.MyHomeFragment;

import java.util.ArrayList;
import java.util.List;

/**ViewPager+Fragment+BottomNavigationView
 * 1.复习ViewPager的用法
 * 2.复习FragmentPagerAdapter的使用
 * 3.BottomNavigationView的使用
 * 4.ViewPager切换页面与BottomNavigationView的联动
 * 5.Badge(新消息提示，图标右上角红圈)
 */

/**
 * 1.准备工作：创建布局并设置底部菜单
 * 2.初始化控件
 * 3.创建FragmentPagerAdapter
 * 4.初始化数据，绑定Adapter和ViewPager
 * 5.viewpager事件监听
 * 6.BottomNavigationView的事件监听
 * 7.创建Badge
 */

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private BottomNavigationView btnNavView;

    private MyFragmentStateVPAdapter myFragmentStateVPAdapter;
    private List<Fragment> fragmentList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        获取控件实例
        viewPager=findViewById(R.id.viewpager);
        btnNavView=findViewById(R.id.button_menu);
        btnNavView.setItemIconTintList(null);//除去自带着色效果，没有这一行图标默认选中是紫色的。
        btnNavView.setItemTextColor(ColorStateList.valueOf(getResources().getColor(R.color.black)));

//        初始化数据
        initData();
//        创建myFragmentStateVPAdapter实例
        myFragmentStateVPAdapter=new MyFragmentStateVPAdapter(getSupportFragmentManager(),fragmentList);
//        绑定Adapter和ViewPager
        viewPager.setAdapter(myFragmentStateVPAdapter);
//        viewpager事件监听
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Toast.makeText(MainActivity.this,"这是第"+(position+1)+"个fragment",Toast.LENGTH_SHORT).show();
//            在这个方法中和导航栏的按钮绑定，fragment页面的改变，导航栏按钮跟随变化
                onPagerSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//         BottomNavigationView的事件监听
        btnNavView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home:
                        viewPager.setCurrentItem(0);//选中第1个Fragment,下标从0开始
                        break;
                    case R.id.bbs:
                        viewPager.setCurrentItem(1);//选中第1个Fragment,下标从0开始
                        break;
                    case R.id.msg:
                        viewPager.setCurrentItem(2);//选中第1个Fragment,下标从0开始
                        break;
                    case R.id.mine:
                        viewPager.setCurrentItem(3);//选中第1个Fragment,下标从0开始
                        break;
                    default:
                        break;
                }
                //记得返回true,响应变化
                return true;
            }
        });
//        创建Badge
        BadgeDrawable badge=btnNavView.getOrCreateBadge(R.id.home);
        badge.setNumber(999);
        badge.setMaxCharacterCount(3);//设置最多显示2个字符
    }

    private void onPagerSelected(int position) {
        switch (position){
            case 0:
                btnNavView.removeBadge(R.id.home);
                btnNavView.setSelectedItemId(R.id.home);
                break;
            case 1:
                btnNavView.removeBadge(R.id.bbs);
                btnNavView.setSelectedItemId(R.id.bbs);
                break;
            case 2:
                btnNavView.removeBadge(R.id.msg);
                btnNavView.setSelectedItemId(R.id.msg);
                break;
            case 3:
                btnNavView.removeBadge(R.id.mine);
                btnNavView.setSelectedItemId(R.id.mine);
                break;
            default:
                break;
        }
    }

    private void initData() {
        fragmentList=new ArrayList<>();
        MyHomeFragment fragment1= MyHomeFragment.newInstance("这是主页fragment");
        MyFragment fragment2=MyFragment.newInstance("这是第2个fragment");
        MyFragment fragment3=MyFragment.newInstance("这是第3个fragment");
        MyFragment fragment4=MyFragment.newInstance("这是第4个fragment");
        fragmentList.add(fragment1);
        fragmentList.add(fragment2);
        fragmentList.add(fragment3);
        fragmentList.add(fragment4);
    }
}