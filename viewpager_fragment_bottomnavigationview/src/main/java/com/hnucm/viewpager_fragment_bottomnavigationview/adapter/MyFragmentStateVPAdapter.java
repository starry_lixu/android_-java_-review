package com.hnucm.viewpager_fragment_bottomnavigationview.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.List;

/**
 * FragmentStatePagerAdapter  回收划过的fragment,适用页面复杂的界面
 * FragmentPagerAdapter  不会回收划过的fragment
 */

public class MyFragmentStateVPAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragmentList;


    public MyFragmentStateVPAdapter(@NonNull FragmentManager fm,
                                    List<Fragment> fragmentList) {
        super(fm);
        this.fragmentList=fragmentList;
    }

//    获取到当前的Fragment
    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragmentList==null?null:fragmentList.get(position);
    }

//    获取到当前有多少Fragment
    @Override
    public int getCount() {
        return fragmentList==null?0:fragmentList.size();
    }


}
