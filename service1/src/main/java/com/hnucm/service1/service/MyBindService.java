package com.hnucm.service1.service;


import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

public class MyBindService extends Service {
    private static final String TAG="MyBindService";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, ":onBind ");
        return new MyBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, ":onCreate ");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, ":onStartCommand ");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, ":onDestroy ");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, ":onUnbind ");
        return super.onUnbind(intent);
    }

//    定义一个MyBinder类在其中定义供服务的启动者可调用的公共方法。
//    这里只是简单的打印输出模拟一下通信
    public class MyBinder extends Binder{
        public void test(){
            Log.d(TAG, "MyBindService中的MyBinder被调用了");
        }
    }
}
