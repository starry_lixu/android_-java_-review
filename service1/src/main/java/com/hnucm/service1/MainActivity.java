package com.hnucm.service1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;

import com.hnucm.service1.service.MyBindService;
import com.hnucm.service1.service.MyService;

public class MainActivity extends AppCompatActivity {

    private MyBindService.MyBinder myBinder=null;
//     绑定式的服务，启动者是可以调用service的,
//     Android 系统创建启动者与服务之间的连接时，
//     会对 ServiceConnection 调用 onServiceConnected()。
//     onServiceConnected() 方法包含一个 IBinder 参数，启动者随后会使用该参数与绑定服务通信。
    private ServiceConnection conn=new ServiceConnection() {

        //todo 服务连接时回调此方法
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            myBinder= (MyBindService.MyBinder) service;
            myBinder.test();
        }

        //todo 服务断开时回调此方法
        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

//    启动服务
    public void startService(View view) {
        Intent intent=new Intent(MainActivity.this, MyService.class);
        startService(intent);
    }
//   绑定服务
    public void bindService(View view) {
        Intent intent=new Intent(MainActivity.this, MyBindService.class);
        bindService(intent,conn,BIND_AUTO_CREATE);
    }
}
